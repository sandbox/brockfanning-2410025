(function ($) {

  Drupal.behaviors.entityPickerIFrame = {

    attach: function (context, settings) {
      // I don't know how to predict what the CSS classes of the Views rows
      // will be, so for now I am trying to catch the standard ones.
      // @TODO: Let other modules hook into this list.
      var selectors = [
        '.views-row',
        '.views-view-grid td',
        '.views-table tr',
        '.masonry-item'
      ];
      selectors = selectors.join(', ');

      var viewResults = settings.entityPicker.viewResults;

      // Attach click behavior to each Views row.
      $(selectors, context).click(function(e) {
        $(selectors, context).removeClass('entity-picker--selected');
        var position = $(selectors, context).index(this);
        if (viewResults[position] !== 'undefined') {
          $(this).addClass('entity-picker--selected');
          $(this).attr('data-entity-picker-id', viewResults[position].id);
          $(this).attr('data-entity-picker-label', viewResults[position].label);
        }
        e.preventDefault();
        e.stopPropagation();
      });
    }
  };
}(jQuery));