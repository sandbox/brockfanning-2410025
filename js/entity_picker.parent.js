(function ($) {

  Drupal.behaviors.entityPickerParent = {

    attach: function (context, settings) {

      var $body = $('body', context);
      var eventFunc = ($.fn.on) ? 'on' : 'bind';

      // React to the entityPickerOpen event.
      $body[eventFunc]('entityPickerOpen', function(e, viewName, displayId) {
        $('#entity-picker').remove();
        var browser = document.createElement('iframe');
        $(browser).hide();
        document.body.appendChild(browser);
        browser.id = 'entity-picker';
        browser.setAttribute('src',
          settings.basePath + 'entity_picker/' + viewName + '/' + displayId);

        var triggeringElement = e.target;

        // Listen for the iframe to finish loading.
        $(browser)[eventFunc]('load', function() {

          var $iframe = $(this).contents();

          // Trigger event so that other scripts can react to the iframe load.
          // Pass in the javascript object for the contents of the iframe, so
          // that other modules can do stuff with it.
          $(triggeringElement).trigger('entityPickerLoaded', [$iframe.get(0)]);

          var selected = null;

          // Attach click behavior to the submit button.
          $iframe.find('#entity-picker--submit')[eventFunc]('click', function() {

            var $selected = $iframe.find('.entity-picker--selected');
            var data = {
              id: $selected.attr('data-entity-picker-id'),
              label: $selected.attr('data-entity-picker-label')
            };
            $(triggeringElement).trigger('entityPickerSubmit', [data]);
            $('#entity-picker').hide();
          });

          // Attach click behavior to the cancel buttons.
          $iframe.find('.entity-picker--cancel')[eventFunc]('click', function() {
            $(triggeringElement).trigger('entityPickerCancel');
            $('#entity-picker').hide();
          });

          // Finally show the picker.
          $(this).fadeIn();
        });
      });
    }
  };
}(jQuery));