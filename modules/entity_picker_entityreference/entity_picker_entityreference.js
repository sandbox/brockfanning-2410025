(function ($) {

  Drupal.behaviors.entityPickerEntityreference = {

    attach: function (context, settings) {

      var eventFunc = ($.fn.on) ? 'on' : 'bind';

      // Look for each entity picker entityreference widget.
      $('.entity-picker--entityreference', context).once('entity-picker--entityreference', function() {

        // Check for the custom data attribute indicating the view+display.
        var viewParts = $(this).attr('data-entity-picker-view').split('|');
        if (typeof viewParts !== 'undefined') {

          // Reference to the input to use inside callbacks.
          var self = this;

          // Stick a browse button after the widget.
          var $button = $('<button>' + Drupal.t('Browse') + '</button>');
          $(this).after($button);

          // On click, signal the entity_picker module to open the picker.
          $button.click(function (e) {

            $(self).trigger('entityPickerOpen', viewParts);
            // Show a loader gif.
            $(this).after('<div class="entity-picker-loading"></div>');
            e.preventDefault();
          });

          // Listen for the entity picker's loaded event to remove the gif.
          $(self)[eventFunc]('entityPickerLoaded', function(e) {
            $('.entity-picker-loading').remove();
          });

          // Listen for the entity_picker's submit event.
          $(self)[eventFunc]('entityPickerSubmit', function(e, info) {
            var newVal = info.label + ' (' + info.id + ')';
            $(self).val(newVal);
          });

          // Add a create button if needed.
          var createPath = $(this).attr('data-entity-picker-create');
          if (typeof createPath !== 'undefined') {

            var $createButton = $('<button>' + Drupal.t('Create') + '</button>');
            $(this).after($createButton);

            // On click, open the entity create page in a new window/tab.
            $createButton.click(function (e) {
              e.preventDefault();
              window.open(Drupal.settings.basePath + createPath);
            })
          }

          // Add an edit button if needed.
          var editPath = $(this).attr('data-entity-picker-edit');
          if (typeof editPath !== 'undefined') {

            var $editButton = $('<button>' + Drupal.t('Edit') + '</button>');
            $(this).after($editButton);

            // On click, open the entity create page in a new window/tab.
            $editButton.click(function (e) {
              e.preventDefault();
              // Get the entity ID from the input value.
              var val = $(self).val();
              var regExp = /\(([^)]+)\)/;
              var matches = regExp.exec(val);
              if (matches) {
                window.open(Drupal.settings.basePath + editPath.replace('[id]', matches[1]));
              }
            })
          }

          // Add a remove button.
          var $removeButton = $('<button>' + Drupal.t('Remove') + '</button>');
          $(this).after($removeButton);

          // On click the remove button clears the input.
          $removeButton.click(function (e) {
            $(self).val('');
            e.preventDefault();
          });
        }
      });
    }
  };
}(jQuery));