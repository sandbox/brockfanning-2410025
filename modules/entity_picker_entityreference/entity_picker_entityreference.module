<?php
/**
 * @file
 * Module code for Entity Picker - Entityreference.
 */

/**
 * Implements hook_field_widget_info().
 */
function entity_picker_entityreference_field_widget_info() {
  $widgets['entity_picker_entityreference'] = array(
    'label' => t('Entity Picker'),
    'description' => t('A single-selection Views-based widget.'),
    'field types' => array(
      'entityreference',
    ),
    'behaviors' => array(
      'multiple values' => FIELD_BEHAVIOR_DEFAULT,
      'default value' => FIELD_BEHAVIOR_NONE,
    ),
    'settings' => array(
      'view' => '',
      'edit_button' => FALSE,
      'create_button' => FALSE,
    ),
  );

  return $widgets;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function entity_picker_entityreference_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  if ($widget['type'] == 'entity_picker_entityreference') {

    $entity_type = $field['settings']['target_type'];
    $info = entity_get_info($entity_type);
    $views = views_get_all_views();

    $options = array();
    foreach ($views as $view) {
      if ($info['base table'] == $view->base_table) {

        $view_name = (empty($view->human_name)) ? $view->name : $view->human_name;
        foreach ($view->display as $display_id => $display) {
          $options[$view->name . '|' . $display_id] = $view_name . ' - ' . $display->display_title;
        }
      }
    }

    $element['view'] = array(
      '#type' => 'select',
      '#title' => t('View'),
      '#description' => t('Pick a View to use for entity selection.'),
      '#options' => $options,
      '#default_value' => isset($settings['view']) ? $settings['view'] : '',
      '#required' => TRUE,
    );

    $element['edit_button'] = array(
      '#type' => 'checkbox',
      '#title' => t('Edit Button'),
      '#description' => t('Display a button for users to edit the currently selected entity.'),
      '#default_value' => isset($settings['edit_button']) ? $settings['edit_button'] : FALSE,
    );

    $element['create_button'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create Button'),
      '#description' => t('Display a button for users to create a new entity of the target type. (Does not work if entityreference field allows multiple bundles.)'),
      '#default_value' => isset($settings['create_button']) ? $settings['create_button'] : FALSE,
    );
  }

  return $element;
}

/**
 * Implements hook_field_widget_form().
 *
 * Most of this copied from entityreference_field_widget_form().
 */
function entity_picker_entityreference_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  // Ensure that the entity target type exists before displaying the widget.
  $entity_info = entity_get_info($field['settings']['target_type']);
  if (empty($entity_info)){
    return array();
  }

  // Also make sure we're in the right place.
  if ($instance['widget']['type'] != 'entity_picker_entityreference') {
    return $element;
  }

  // Use entityreference's "selection handler".
  $host_entity_type = $instance['entity_type'];
  $host_entity = isset($element['#entity']) ? $element['#entity'] : NULL;
  $handler = entityreference_get_selection_handler($field, $instance, $host_entity_type, $host_entity);

  // See if we have an existing value.
  $default_value = NULL;
  if (isset($items[$delta])) {
    // If so, get the default value.
    $item = $items[$delta];
    $entity_id = $item['target_id'];
    $entity = entity_load_single($field['settings']['target_type'], $entity_id);
    $label = $handler->getLabel($entity);
    $default_value = "$label ($entity_id)";
  }

  // Display a readonly input.
  $element += array(
    '#type' => 'textfield',
    '#default_value' => $default_value,
    '#attributes' => array(
      'readonly' => 'readonly',
      'class' => array(
        'entity-picker--entityreference',
      ),
      'data-entity-picker-view' => $instance['widget']['settings']['view'],
    ),
    '#element_validate' => array('_entityreference_autocomplete_validate'),
  );

  $target_type = $field['settings']['target_type'];

  // If necessary, add an attribute to trigger a create button.
  if ($instance['widget']['settings']['create_button']) {
    // Unfortunately there is nothing in entity_get_info to tell us the
    // path for creating an entity. We assume [entity_type]/add, and allow
    // for special cases for the more common cases.
    $create_path = $target_type . '/add';
    $bundle = FALSE;
    if (!empty($field['settings']['handler_settings']['target_bundles'])) {
      // We only care about a single bundle, because multiple is impossible
      // here.
      if (count($field['settings']['handler_settings']['target_bundles']) == 1) {
        $bundle = array_shift($field['settings']['handler_settings']['target_bundles']);
      }
    }
    // Node and Taxonomy depend on bundles
    if ($bundle && ('node' == $target_type || 'taxonomy_term' == $target_type)) {
      if ('node' == $target_type) {
        $create_path = 'node/add/' . $bundle;
      }
      elseif ('taxonomy_term' == $target_type) {
        $create_path = 'admin/structure/taxonomy/' . $bundle . '/add';
      }
    }
    // Also support users.
    elseif ('user' == $target_type) {
      $create_path = 'admin/people/create';
    }

    $element['#attributes']['data-entity-picker-create'] = htmlentities($create_path);
  }

  // If necessary, add a trigger to create an edit button.
  if ($instance['widget']['settings']['edit_button']) {
    // Unfortunately there is nothing in entity_get_info to tell us the
    // path for editing an entity. We will assume [type]/[id]/edit, with
    // some exceptions.
    $edit_path = $target_type . '/[id]/edit';
    if ('taxonomy_term' == $target_type) {
      $edit_path = 'taxonomy/term/[id]/edit';
    }
    $element['#attributes']['data-entity-picker-edit'] = htmlentities($edit_path);
  }

  // Attach the Entity Picker js/css.
  entity_picker_attach_assets($element);
  // Attach our own js and css.
  $path = drupal_get_path('module', 'entity_picker_entityreference');
  $element['#attached']['js'][] = array(
    'type' => 'file',
    'data' => $path . '/entity_picker_entityreference.js',
  );
  $element['#attached']['css'][] = array(
    'type' => 'file',
    'data' => $path . '/entity_picker_entityreference.theme.css',
  );

  return array('target_id' => $element);
}
