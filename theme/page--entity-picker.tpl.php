<?php
/**
 * @file
 * A stripped-down version of page.tpl.php for the entity_picker Views modal.
 */
?>
<div class="entity-picker--wrapper">
  <a class="entity-picker--cancel close"><?php print t('Close') ?></a>
  <?php print render($page['content']) ?>
  <button id="entity-picker--submit"><?php print t('Submit') ?></button>
  <button class="entity-picker--cancel"><?php print t('Cancel') ?></button>
</div>