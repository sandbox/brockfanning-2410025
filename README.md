# Entity Picker

This module is in the spirit of the D8 [Entity Browser](http://drupal.org/project/entity_browser) project, in that it provides a decoupled browser for entities. However this has a much smaller scope, currently limited to single-selection and requiring a [View](http://drupal.org/project/views) to be used as the browser.

This module includes a submodule called "entity_picker_entityreference" which provides an integration with [Entity Reference](http://drupal.org/project/entityreference) fields.

This module doesn't do anything by itself. It only provides the entity picker for other modules.

## Installation

Download and install the module as normal.

## Usage by other modules

The "entity picker" is javascript-based, and should be used with javascript events. These are the events to be used, along with jQuery examples.

'entityPickerOpen': This event will open the entity picker. It should be triggered with an array including the view name and the view display id. For example:

```
$(myElement).trigger('entityPickerOpen', ['my_view', 'my_display']);
```

'entityPickerLoaded': This event will be triggered when the entity picker has finished loading. It passes the dom element for the entity picker contents. For example:

```
$(myElement).on('entityPickerLoaded', function(e, picker) {
  console.log('Entity picker loaded...');
  $(picker).find('.some-selector').each(function(e) {
    // Do something
  });
});
```

'entityPickerCancel': This event is triggered by the entity picker when the user closes/cancels selection. You don't necessarily need to do anything with this, but it's there if you need it. For example:

```
$(myElement).on('entityPickerCancel', function(e) {
  console.log('Closing entity picker...');
});
```

'entityPickerSubmit': This event is triggered by the entity picker when the user submits a selection. It passes an info object that includes the entity's id and label. For example:

```
$(myElement).on('entityPickerSubmit', function(e, info) {
  console.log('Entity id is ' + info.id);
  console.log('Entity label is ' + info.label);
});
```

## Todo

* General testing, especially with various contrib Views row styles