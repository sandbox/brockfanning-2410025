<?php
/**
 * @file
 * Page callbacks for Entity Picker.
 */

/**
 * Page callback for the Entity Picker page.
 */
function entity_picker_view($view_name, $display_id) {

  // Prevent responsible modules from adding stuff to the DOM through js.
  module_invoke_all('suppress');

  // Load our view.
  $view = views_embed_view($view_name, $display_id);
  if (!empty($view)) {

    // Output the view along with CSS and JS.
    $path = drupal_get_path('module', 'entity_picker');
    return array(
      '#markup' => $view,
      '#attached' => array(
        'js' => array(
          array(
            'data' => $path . '/js/entity_picker.iframe.js',
            'type' => 'file',
          ),
        ),
        'css' => array(
          array(
            'data' => $path . '/css/entity_picker.iframe.theme.css',
            'type' => 'file',
          ),
        ),
      ),
    );
  }
  return '';
}